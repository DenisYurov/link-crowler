package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

//object Link
public class Link {
    private final StringProperty url = new SimpleStringProperty();;
    private final StringProperty checkResult = new SimpleStringProperty();;

    public Link(String ur, String cResult) {
        url.set(ur);
        checkResult.set(cResult);
    }

    public String getUrl() {
        return url.get();
    }
    public void setUrl(String ur) {
        checkResult.set(ur);
    }

    public String getCheckResult() {
        return checkResult.get();
    }
    public void setCheckResult(String cResult) {
        checkResult.set(cResult);
    }

    public StringProperty urlProperty() {
        return url;
    }

    public StringProperty checkResultProperty() {
        return checkResult;
    }

}
