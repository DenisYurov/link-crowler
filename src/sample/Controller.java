package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.Iterator;
import java.util.Scanner;

public class Controller {
    //final ObservableList<Link> links = FXCollections.observableArrayList();


    public Controller() {
        //test purpose
/*
        Main.links.add(new Link("http://www.fs1inc.com/engine-computer-programmed-plug-play-1997-dodge-truck-56040395aa-05014152aa-3-9l-ecu-ecm-pcm-oem", ""));
        Main.links.add(new Link("http://www.fs1inc.com/engine-computer-programmed-plug-play-2000-dodge-truck-56040400ac-05018488aa-5-9l-ecu-ecm-pcm", ""));
        Main.links.add(new Link("http://www.fs1inc.com/engine-computer-programmed-plug-play-2000-dodge-ram-van-56040404ab-05018488aa-5-9l-ecu-ecm-pcm", ""));
        Main.links.add(new Link("http://www.fs1inc.com/engine-computer-programmed-plug-play-2000-dodge-durango-56040408ae-05018488aa-5-9l-ecu-ecm-pcm", ""));
        if (Main.textWanted == null || Main.textWanted.length() == 0) {
            Main.textWanted = "Welcome to Flagship One, Inc.!||56040395AA || 2000 Dodge Ram Truck 5.9L";
        }
        */
    }

    public void initialize() {
        this.links();
    }

    @FXML
    ProgressBar progressBar;
    @FXML
    ListView<String> listView;
    @FXML
    Label progressLabel;
    @FXML
    BorderPane content;
    @FXML
    Button startBtn;

    @FXML
    protected void locateFile(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(new Stage());
        Main.links.clear();
        if (file != null) {
            try {
 /*
                BufferedReader in = new BufferedReader
                        (new FileReader(file.toString()));
*/
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                new FileInputStream(file.toString()), "UTF8"));

                Main.links.clear();
                String s;
                while ((s = in.readLine()) != null) {
                    if (s.trim().length() > 0) {
                        Main.links.add(new Link(s, ""));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        progressBar.progressProperty().unbind();
        progressBar.setProgress(0);
        links();
    }

    protected Task<Void> task;

    @FXML
    protected void scanAction() {
        if (task == null || !task.isRunning()) {
            startBtn.setText("Stop");
            task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    String[] textWantedArray = parseTextWanted();
                    if (textWantedArray == null) {
                        updateMessage("No text wanted");
                        return null;
                    }
                    float delta = (float) 1 / Main.links.size();
                    float progress = 0;
                    updateMessage("Running");
                    for (Link s : Main.links) {
                        String out = null;
                        try {
                            //update progressbar position
                            progress = progress + delta;
                            updateProgress(progress, 1);
                            //send HTTP reguest and get answer
                            InputStream inpStream = new URL(s.getUrl()).openStream();

                            out = new Scanner(inpStream, "UTF-8").useDelimiter("\\A").next();
                            s.setCheckResult(pageCrowl(out, s.getUrl(), textWantedArray));
                            System.out.println(s.getUrl()+" "+s.getCheckResult());
                        } catch (java.net.UnknownHostException e) {
                            System.out.println("Unknown Host Ex");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (isCancelled()) {
                            updateMessage("Cancelled");
                            break;
                        }
                    }
                    updateMessage("Complete");
                    return null;
                }
            };
            // bind "late" to not get an initial indeterminate PB
            progressBar.progressProperty().bind(task.progressProperty());
            progressLabel.textProperty().bind(task.messageProperty());
            //listen when task will finish
            task.messageProperty().addListener((obs, oldMsg, newMsg) -> {
                if (newMsg == "Complete" || newMsg == "Cancelled") {
                    startBtn.setText("Start");
                }
            });
            new Thread(task).start();

        } else {
            task.cancel();
            progressBar.progressProperty().unbind();
            progressLabel.textProperty().unbind();
            progressBar.setProgress(0);
            progressLabel.setText("Cancelled");
        }
    }

    //loading "Phrases" page
    @FXML
    protected void textWanted(ActionEvent event) {
        try {
            TextArea root = FXMLLoader.load(getClass().getResource("phrases.fxml"));
            content.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //loading "Links" page
    @FXML
    private void links() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("linkst.fxml"));
            Parent root = loader.load();
            //Linkst controller = loader.getController();
            //controller.setContext();
            content.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //loading "Help" page
    @FXML
    protected void help(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("help.fxml"));
            content.setCenter(root);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void copyToClipBoard() {
        StringBuilder clipboardString = new StringBuilder();
        for (Link s : Main.links) {
            clipboardString.append(s.getUrl());
            clipboardString.append(s.getCheckResult());
            clipboardString.append(System.getProperty("line.separator"));
        }
        final ClipboardContent content = new ClipboardContent();
        content.putString(clipboardString.toString());
        Clipboard.getSystemClipboard().setContent(content);
    }

    //split text wanted on phrases by delimiter
    private String[] parseTextWanted() {
        if (Main.textWanted != null && Main.textWanted.length() > 0) {
            return Main.textWanted.trim().split("\\|\\|");
        } else {
            return null;
        }
    }

    // looking for text wanted in page source
    private String pageCrowl(String pageContext, String url, String[] textWantedArray) {
        String result = "";
        for (String textWanted : textWantedArray) {
            if (pageContext.toLowerCase().contains(textWanted.toLowerCase())) {
                result = result + " || '" + textWanted + "' is found";
            }else{
                result = result + " || '" + textWanted + "' not found";
            }
        }
        return result;
    }
}
