package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.*;


public class Main extends Application {
    public static String textWanted;
    public static ObservableList<Link> links = FXCollections.observableArrayList();
    private static String tempFIle = System.getProperty("java.io.tmpdir") + "linkChecker.txt";

    @Override
    public void start(Stage primaryStage) throws Exception {
        readTextWanted();
        Parent root = FXMLLoader.load(getClass().getResource("mainform.fxml"));
        primaryStage.setTitle("Link checker");
        primaryStage.setScene(new Scene(root, 713, 450));
        primaryStage.setMinWidth(713);
        primaryStage.setMinHeight(490);
        primaryStage.show();
    }

    @Override
    public void stop() {
        saveTextWanted();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private void saveTextWanted()

    {
        try {

            File myFile = new File(tempFIle);
            System.out.println(tempFIle);
            // check if file exist, otherwise create the file before writing
           // if (!myFile.exists()) {
                myFile.createNewFile();
           // }
            Writer writer = new FileWriter(myFile);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(textWanted);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readTextWanted() {
        File f = new File(tempFIle);
        if (f.exists() && !f.isDirectory()) {
            try {
                BufferedReader in = new BufferedReader
                        (new FileReader(tempFIle));
                String s;
                textWanted ="";
                while ((s = in.readLine()) != null) {
                    if (s.trim().length() > 0) {
                        textWanted = textWanted + s;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
