package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;


public class Links {
    @FXML
    ListView<String> listView;
    private ObservableList<String> urls = FXCollections.observableArrayList();
    public void setContext(ObservableList<String> urls) {
        this.urls = urls;
        listView.setItems(this.urls);
    }

}
