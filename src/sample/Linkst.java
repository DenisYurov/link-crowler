package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class Linkst {
    @FXML
    TableView linksTable;
    @FXML
    TableColumn urlCol;
    @FXML
    TableColumn checkResultCol;

        public void initialize() {
        urlCol.setCellValueFactory(
                new PropertyValueFactory<Link, String>("url")
        );
        checkResultCol.setCellValueFactory(
                new PropertyValueFactory<Link, String>("checkResult")
        );
        urlCol.prefWidthProperty().bind(linksTable.widthProperty().multiply(0.74));
        checkResultCol.prefWidthProperty().bind(linksTable.widthProperty().multiply(0.25));
        linksTable.setItems(Main.links);
    }
}
