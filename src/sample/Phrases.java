package sample;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

public class Phrases {
    @FXML
    private TextArea twTextArea;

    public String getContext() {
        return twTextArea.getText();
    }

    @FXML
    public void initialize() {
        twTextArea.setText(Main.textWanted);
        //adding listener to focusProperty
        twTextArea.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            focusState(newValue);
        });
    }

    private void focusState(boolean value) {
        if (value) {
            //System.out.println("Focus Gained");
        } else {
            //System.out.println("Focus Lost");
            Main.textWanted = twTextArea.getText();

        }
    }
}


