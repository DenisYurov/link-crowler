package sample;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

public class Help {
    @FXML
    private TextArea hlTextArea;

    public void initialize() {
        String s = "Please insert in edit -> text wanted -> text area, phrases that you would like find in web pages." +System.getProperty("line.separator")+
                   "You should take phrases for search from page sources. " +System.getProperty("line.separator")+
                   "Phrases taken from page shown in browser page not always looks same as in source of same page." +System.getProperty("line.separator")+
                   "If you would like check many phrases you should put || delimiter between phrases."+System.getProperty("line.separator");
        hlTextArea.setText(s);

    }
}
